# Segis-Net

Scripts involved in:
Li et al., Longitudinal diffusion MRI analysis using Segis-Net: a single-step deep-learning framework for simultaneous segmentation and registration (NeuroImage 2021).

Experiments in the paper were implemented using Python-3.6.7, Keras2.2.0 with a Tensorflow-1.4.0 backend.
